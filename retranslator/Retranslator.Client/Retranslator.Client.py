from socket import *
import pyaudio

# socket
HOST = '0.0.0.0'
PORT = 9000
address = (HOST, PORT)

# audio
FORMAT = pyaudio.paInt16
CHUNK = 1024
CHANNELS = 1
RATE = 8000
data = ''


def main():
    global data
    # create socket, broadcast mode
    client = socket(AF_INET, SOCK_DGRAM)
    client.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

    p = pyaudio.PyAudio()

    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    output=True,
                    frames_per_buffer=CHUNK,
                    )

    client.bind((HOST, PORT))
    data, client_ip = client.recvfrom(CHUNK)

    while data != '':
        data = ''
        answer = client.recvfrom(CHUNK)
        data = answer[0]
        client_ip = answer[1]
        
        if data == 'stop':
            data = ''
            break

        stream.write(data)
        print 'Receive from ' + client_ip[0] + ': ' + str(len(data)) + ' bytes'

    print 'Stop receive from client'
    stream.stop_stream()
    stream.close()
    p.terminate()
    client.close()


if __name__ == "__main__":

    print 'Listening for data on port %(PORT)s -- press Ctrl-C to stop' % {'PORT': PORT}

    while True:
        main()

#import socket
#import pyaudio

#HOST = '0.0.0.0'  # Symbolic name meaning all available interfaces
#PORT = 5555        # Arbitrary non-privileged port
#frames = []

#if __name__ == "__main__":
#    FORMAT = pyaudio.paInt16
#    CHUNK = 65536
#    CHANNELS = 1
#    RATE = 8000

#    p = pyaudio.PyAudio()

#    stream = p.open(format=FORMAT,
#                    channels = CHANNELS,
#                    rate = RATE,
#                    output = True,
#                    frames_per_buffer = CHUNK,
#                    )

#    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#    udp.bind((HOST, PORT))
#    data = udp.recv(CHUNK)

 
#    while data is not None:
#        data = None
#        data = udp.recv(CHUNK)
#        stream.write(data)
#        print 'Receive ' + str(len(data))
