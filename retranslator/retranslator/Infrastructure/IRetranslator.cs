﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace retranslator.Infrastructure
{
    public interface IRetranslator
    {
        bool State { get; set; }

        void Start();

        void Stop();
    }
}
