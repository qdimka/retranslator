﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace retranslator.ViewModels
{
    public class MainViewModel
    {
        private SettingsViewModel settingsViewModel;

        private RetranslatorViewModel retranslatorViewModel;


        public SettingsViewModel SettingsViewModel => settingsViewModel;

        public RetranslatorViewModel RetranslatorViewModel => retranslatorViewModel;

        public MainViewModel()
        {
            retranslatorViewModel = new RetranslatorViewModel();
            settingsViewModel = new SettingsViewModel();   
        }
    }
}
