﻿using Commands;
using NAudio.Wave;
using retranslator.Infrastructure;
using retranslator.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Input;

namespace retranslator.ViewModels
{
    public class RetranslatorViewModel : ViewModelBase
    {
        private bool state;
        private int numBytesSend;
        private int bufferSize = 1024;

        private Settings settings;
        private UdpClient server;
        private WaveIn input;
        private IPEndPoint remote_point;

        private readonly ICommand start;

        public RetranslatorViewModel()
        {
            settings = new Settings();
            start = new DelegateCommand(StartRecording);

        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public bool State
        {
            get { return state; }
            set
            {
                state = value;
                NotifyPropertyChanged("State");
            }
        }

        public int Bytes
        {
            get { return numBytesSend; }
            set
            {
                numBytesSend = value;
                NotifyPropertyChanged("Bytes");
            }
        }

        public ICommand Start => start;


        private void voiceInput(object sender, WaveInEventArgs e)
        {
            try
            {
                //Разбиваем на меньшие кусочки
                List<byte> buffer = new List<byte>();

                bufferSize = e.Buffer.Length / 4;

                for (int i = 0; i < e.Buffer.Length; i++)
                {
                    buffer.Add(e.Buffer[i]);

                    if (buffer.Count == bufferSize)
                    {
                        var b = buffer.ToArray();
                        server.Send(b, b.Length, remote_point);
                        buffer.Clear();
                    }
                }

                Bytes += e.Buffer.Length;
            }
            catch (Exception ex)
            {

            }
        }

        private void StartRecording()
        {
            try
            {
                if (!state)
                {
                    Connect(50);
                }
                else
                {
                    Disconnect();
                }
            }
            catch
            {

            }
        }

        private void Connect(int buffer)
        {
            //udp 
            server = new UdpClient();
            server.EnableBroadcast = true;
            remote_point = new IPEndPoint(IPAddress.Broadcast, Int32.Parse(settings.Port));

            //NAudio
            input = new WaveIn();
            //определяем его формат - частота дискретизации 8000 Гц, ширина сэмпла - 16 бит, 1 канал - моно
            input.WaveFormat = new WaveFormat(8000, 16, 1);
            input.DataAvailable += voiceInput;
            input.StartRecording();

            State = true;
        }

        private void Disconnect()
        {
            if (!State)
                return;

            input.DataAvailable -= voiceInput;
            input.StopRecording();
            input.Dispose();

            var msg = System.Text.Encoding.UTF8.GetBytes("stop");
            server.Send(msg, msg.Length, remote_point);
            server.Close();

            State = false;
        }
    }
}
