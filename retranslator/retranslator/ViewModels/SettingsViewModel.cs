﻿using Commands;
using NAudio.Wave;
using retranslator.Infrastructure;
using retranslator.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace retranslator.ViewModels
{
    public class SettingsViewModel:ViewModelBase
    {
        private string ip;
        private string port;
        private List<string> devices;
        private string selectedItem;

        private readonly ICommand saveSettings;
        private Settings settings;

        public SettingsViewModel()
        {
            devices = new List<string>();
            settings = new Settings();

            Devices = GetAudioDevices();
            IP = settings.IP;
            Port = settings.Port;
            SelectedItem = settings.Selected;

            saveSettings = new DelegateCommand(save);

            //ISettings settings;
        }

        public string SelectedItem
        {
            get { return selectedItem; }
            set
            {
                selectedItem = value;
                NotifyPropertyChanged("SelectedItem");
            }
        }

        public ICommand SaveSettings => saveSettings;

        public List<String> Devices
        {
            get { return devices; }
            set
            {
                devices = value;
                NotifyPropertyChanged("Devices");
            }
        }

        public string Port
        {
            get { return port; }
            set
            {
                port = value;
                NotifyPropertyChanged("Port");
            }
        }

        public string IP
        {
            get { return ip; }
            set
            {
                ip = value;
                NotifyPropertyChanged("IP");
            }
        }

        private List<string> GetAudioDevices()
        {
            var tmp = new List<string>();
            int waveInDevices = WaveIn.DeviceCount;
            for (int waveInDevice = 0; waveInDevice < waveInDevices; waveInDevice++)
            {
                WaveInCapabilities deviceInfo = WaveIn.GetCapabilities(waveInDevice);
                tmp.Add(deviceInfo.ProductName);
            }

            return tmp;
        }

        private void save()
        {
            settings.IP = this.IP;
            settings.Port = this.Port;
            settings.Selected = this.SelectedItem;
            settings.Save();
        }

        private string[] ToStringArray(List<string> list)
        {
            var array = new String[list.Count];
            for (int i = 0; i < array.Count(); i++)
            {
                array[i] = list[i];
            }

            return array;
        }
    }
}
