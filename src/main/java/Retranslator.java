import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;
import java.io.InputStream;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by qdimk on 08.12.2016.
 */
public class Retranslator {

    public static void main(String[] args) {
        String host = "localhost";

        if (args.length>0) {
            host = args[0];
        }

        AudioFormat format = new AudioFormat(8000.0f, 16, 1, true, false);
        //AudioFormat format = new AudioFormat(8000.0f, 8, 1, true, false);
        SourceDataLine speakers;

        try {
            InetAddress ipAddr = InetAddress.getByName(host);

            DatagramSocket s = new DatagramSocket(5555);
            //Socket s = new Socket(ipAddr, 5555);

            InputStream is = s.getInputStream();

            DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, format);
            speakers = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
            speakers.open(format);
            speakers.start();

            int numBytesRead;

            byte[] data = new byte[204800];

            while (true) {
                numBytesRead = is.read(data);
                speakers.write(data, 0, numBytesRead);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}